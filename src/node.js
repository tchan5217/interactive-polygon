import EventEmitter from 'wolfy87-eventemitter'

class BaseNode {
  constructor () {
    this.nodes = []
  }

  find = (condition) => {
    let target

    const find = (node) => {
      if (node.nodes) node.nodes.map(n => find(n))
      else {
        if (!target) {
          if (condition(node)) target = node
        }
      }
    }

    find(this)

    return target
  }
}

class Node extends BaseNode {
  constructor () {
    super()
    this.canvas = null
    this.ctx = null
    const ee = new EventEmitter()
    this.on = (e, values) => ee.on(e, values)
    this.off = (e, values) => ee.off(e, values)
    this.once = (e, values) => ee.once(e, values)
    this.emit = (e, values) => ee.emit(e, values)
  }
}

export {
  Node as default,
  Node,
  BaseNode
}
