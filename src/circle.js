import { dist, isMobileDevice } from './utils'

export default class Circle {
  constructor (options) {
    this.ctx = options.ctx

    const {
      position = { x: -1, y: -1 },
      color = '#15C95D',
      radius = 4,
      border = 2,
      is
    } = options

    this.position = position
    this.color = color
    this.radius = radius
    this.border = border
    this.state = 'default'
    this.is = is
  }

  above = point => {
    const d = dist(point, this.position)
    const r = this.radius + this.border * 1.75
    let offset = Math.max(isMobileDevice ? 40 : 24, r * 2)
    return Boolean(d <= offset)
  }

  render = () => {
    const { x, y } = this.position
    if (x < 0 || y < 0) return

    if (this.state === 'focus') {
      this.dot(x, y, this.radius + this.border * 1.75, this.color)
      this.dot(x, y, this.radius + this.border, 'white')
      this.dot(x, y, this.radius, this.color)
    } else if (this.state === 'hover') {
      this.dot(x, y, this.radius + this.border * 1.75, this.color)
      this.dot(x, y, this.radius, 'white')
    } else {
      this.dot(x, y, this.radius + this.border * 1.75, 'white')
      this.dot(x, y, this.radius + this.border, this.color)
      this.dot(x, y, this.radius, 'white')
    }
  }

  dot = (x, y, radius, color) => {
    const ctx = this.ctx
    ctx.beginPath()
    ctx.fillStyle = color
    ctx.arc(x, y, radius, 0, Math.PI * 2, null)
    ctx.fill()
    ctx.closePath()
  }
}
