import { isMobileDevice, dist, isTouches, isLeftButton } from './utils'
import EventEmitter from 'wolfy87-eventemitter'

const dragDist = 0.1

class Gestures {
  constructor (canvas) {
    this.canvas = canvas
    this.domElement = canvas.domElement
    this.anchor = null
    this.hovering = null

    const ee = new EventEmitter()
    this.on = (e, values) => ee.on(e, values)
    this.off = (e, values) => ee.off(e, values)
    this.once = (e, values) => ee.once(e, values)
    this.emit = (e, values) => ee.emit(e, values)

    this.initEvts()
  }

  initEvts = () => {
    if (isMobileDevice) {
      this.domElement.addEventListener('touchmove', this.move)
      this.domElement.addEventListener('touchend', this.up)
      this.domElement.addEventListener('touchstart', this.down)
    } else {
      this.domElement.addEventListener('mousemove', this.move)
      this.domElement.addEventListener('mouseup', this.up)
      this.domElement.addEventListener('mousedown', this.down)
    }
  }

  destruct = () => {
    if (isMobileDevice) {
      this.domElement.removeEventListener('touchmove', this.move)
      this.domElement.removeEventListener('touchend', this.up)
      this.domElement.removeEventListener('touchstart', this.down)
    } else {
      this.domElement.removeEventListener('mousemove', this.move)
      this.domElement.removeEventListener('mouseup', this.up)
      this.domElement.removeEventListener('mousedown', this.down)
    }

    this.anchor = null
    this.hovering = null
  }

  detectHover = pt => {
    if (this.focusing) return

    let hovering = this.canvas.find(n => n.above(pt))

    if (hovering) {
      hovering.state = 'hover'
      this.hovering = hovering
    } else {
      if (this.hovering) this.hovering.state = 'default'
      this.hovering = null
    }
  }

  down = e => {
    if (!(isTouches(e) || isLeftButton(e))) return

    const pt = this.toRelativePos(e)
    this.detectHover(pt)
    if (this.hovering) {
      this.focusing = this.hovering
      this.focusing.state = 'focus'
    }

    this.anchor = pt
    this.emit('down', { target: this.hovering, position: this.anchor })
  }

  up = e => {
    if (!(isTouches(e) || isLeftButton(e))) return

    const pt = this.toRelativePos(e)
    const isClick = dist(this.anchor, pt) < dragDist

    if (isClick) this.emit('click', { target: this.hovering, position: pt })
    else if (this.focusing) {
      this.emit('dragend', { target: this.focusing, position: pt })
    }

    this.emit('up', { target: this.hovering, position: pt })

    if (this.focusing) {
      this.focusing.state = isMobileDevice ? 'default' : 'hover'
      this.focusing = null
    }

    this.anchor = null
  }

  move = e => {
    const pt = this.toRelativePos(e)
    this.detectHover(pt) // do not update hovering b4 up // on drag element changed
    this.emit('move', { target: this.hovering, position: pt })

    if (!this.anchor) return
    const isClick = dist(this.anchor, pt) < dragDist

    if (
      !isClick &&
      this.focusing
    ) this.emit('drag', { target: this.focusing, position: pt })
  }

  toRelativePos = e => {
    const rect = this.domElement.getBoundingClientRect()

    const x = e.clientX || (e.touches[0] && e.touches[0].clientX) || e.changedTouches[0].clientX
    const y = e.clientY || (e.touches[0] && e.touches[0].clientY) || e.changedTouches[0].clientY

    return {
      x: x - rect.left,
      y: y - rect.top
    }
  }
}

export default Gestures
