import Gestures from './gestures'
import { BaseNode } from './node'

export default class Canvas extends BaseNode {
  constructor (el, renderTo = el) {
    super()

    if (renderTo.tagName !== 'CANVAS') {
      throw new Error('Canvas required')
    }

    this.domElement = el
    this.renderTo = renderTo
    this.ctx = renderTo.getContext('2d')

    this.gestures = new Gestures(this)
    this.on = this.gestures.on
    this.off = this.gestures.off
    this.once = this.gestures.once
  }

  render = () => {
    const { ctx, renderTo } = this
    ctx.clearRect(0, 0, renderTo.clientWidth, renderTo.clientHeight)
    if (this.nodes.length < 1) return

    this.nodes.forEach(node => node.render())

    if (typeof this.effect === 'function') this.effect()
  }

  begin = () => {
    const loop = () => {
      this.render()
      window.requestAnimationFrame(loop)
    }
    loop()
  }

  addNode = (node) => {
    node.canvas = this
    node.ctx = this.ctx
    if (typeof node.init === 'function') node.init()
    this.nodes.push(node)
  }

  removeNode = (node) => {
    const i = this.nodes.findIndex(n => n === node)
    if (typeof node.destruct === 'function') node.destruct()
    this.nodes.splice(i, 1)
  }

  clear = () => {
    this.nodes.length = 0
    this.effect = null
  }

  destruct = () => {
    this.gestures.destruct()

    this.nodes.map(node => {
      if (typeof node.destruct === 'function') {
        node.destruct()
      }
    })

    this.clear()
  }
}
