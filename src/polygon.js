import Circle from './circle'
import Interactable from './interactable'

class Polygon extends Interactable {
  constructor (props) {
    super(props)

    const superDestruct = this.destruct.bind(this)

    this.destruct = () => {
      this.state = 'done'
      this.canvas.off('click', this.interactiveAdd)
      this.canvas.off('move', this._onMouseMoveEffect)
      this.effect = null
      this.canvas.effect = null

      superDestruct()
    }
  }

  interactiveAdd = (e) => {
    const { target, position } = e

    if (target) {
      if (target === this.vertices[0] && this.vertices.length > 2) {
        this.add(target.position, this.vertices[0])
        this.emit('close', { target: this })
      }
    } else {
      this.add(position)
    }
  }

  _onMouseMoveEffect = e => {
    this.effect = () => {
      this.whitelineEffect(e.position, n => (
        this.vertices[0] === n &&
        this.vertices.length > 2
      ))
    }
  }

  draw = () => {
    this.once('close', () => {
      this.state = 'done'
      this.canvas.off('click', this.interactiveAdd)
      this.canvas.off('move', this._onMouseMoveEffect)
      this.effect = null

      // https://stackoverflow.com/a/1756367
      // bug if dev tool is opened, check this answer comments
      const el = this.canvas.domElement
      el.style.cursor = 'grab'
    })

    this.state = 'drawing'
    this.canvas.on('click', this.interactiveAdd)
    this.canvas.on('move', this._onMouseMoveEffect)
  }

  pinDraw = () => {
    const el = this.canvas.domElement
    const style = window.getComputedStyle(el)
    const pt = { x: (parseFloat(style.width) / 2), y: (parseFloat(style.height) / 2) }

    const center = new Circle({
      position: pt,
      ctx: this.ctx,
      color: 'red',
      radius: this.radius,
      border: this.border
    })

    this.effect = () => {
      const target = this.whitelineEffect(pt, n => (
        this.vertices[0] === n &&
        this.vertices.length > 2
      ))

      this.nodes.forEach(n => {
        if (n === target) n.color = 'red'
        else n.color = this.color
      })

      if (target) center.state = 'focus'
      else center.state = 'default'
    }

    this.canvas.effect = () => {
      center.render()
    }

    const onClick = this.interactiveAdd

    this.state = 'drawing'
    onClick({ position: pt })

    this.once('close', e => {
      this.state = 'done'
      this.effect = null
      this.canvas.effect = null
      this.nodes.forEach(n => { n.color = this.color })
    })

    return () => {
      if (this.state === 'done') return
      let target = this.find(n => n.above(pt))
      const e = { target, position: pt }
      onClick(e)
    }
  }
}

export default Polygon
