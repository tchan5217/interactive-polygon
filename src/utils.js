export const isMobileDevice = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

export const isLeftButton = e => (
  e.button === 0
)

export const isTouches = e => window.TouchList && (
  e.touches instanceof TouchList ||
  e.changedTouches instanceof TouchList
)

export const dist = (p1, p2) => {
  const { x: x1, y: y1 } = p1
  const { x: x2, y: y2 } = p2
  const dx = x2 - x1
  const dy = y2 - y1
  const dist = Math.sqrt(dx * dx + dy * dy)
  return dist
}

export const midpoint = (p1, p2) => {
  const { x: x1, y: y1 } = p1
  const { x: x2, y: y2 } = p2
  return {
    x: (x1 + x2) / 2,
    y: (y1 + y2) / 2
  }
}
