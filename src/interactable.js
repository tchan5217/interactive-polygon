import Circle from './circle'
import { midpoint } from './utils'
import Node from './node'

export default class Interactable extends Node {
  constructor (options = {}) {
    super()

    this.canvas = null
    this.ctx = null
    this.vertices = []
    this.midpoints = []
    this.state = 'empty' // empty / drawing / done
    this.effect = null

    // options
    this.color = options.color || '#15C95D'
    this.radius = options.radius || 4
    this.border = options.border || 2
    this.lineWidth = options.lineWidth || 4
  }

  onDrag = e => {
    if (this.state !== 'done') return
    const { target, position } = e
    target.position = position
    this.changed()
  }

  onDown = e => {
    if (this.state !== 'done') return
    const target = e.target && this.find(n => n === e.target)
    if (target && target.is === 'midpoint') {
      this.insert(target)
    }
  }

  onCursorMove = e => {
    if (this.state !== 'done') return
    const el = this.canvas.domElement
    if (
      e.target &&
      this.nodes.some(n => n === e.target)
    ) {
      if (el.style.cursor === 'default') {
        el.style.cursor = 'grab'
      }
    } else {
      el.style.cursor = 'default'
    }
  }

  onCursorPress = (action) => e => {
    if (this.state !== 'done') return
    const el = this.canvas.domElement

    if (
      e.target &&
      (
        this.vertices.some(n => n === e.target) ||
        e.target.is === 'midpoint'
      )
    ) {
      if (action === 'down') el.style.cursor = 'grabbing'
      else if (action === 'up') el.style.cursor = 'grab'
    }
  }

  onCursorUp = this.onCursorPress('up')
  onCursorDown = this.onCursorPress('down')

  init = () => {
    this.canvas.on('drag', this.onDrag)
    this.canvas.on('down', this.onDown)
    this.canvas.on('move', this.onCursorMove)
    this.canvas.on('down', this.onCursorDown)
    this.canvas.on('up', this.onCursorUp)
    this.canvas.on('click', this.onCursorMove)
  }

  destruct = () => {
    this.canvas.off('drag', this.onDrag)
    this.canvas.off('down', this.onDown)
    this.canvas.off('move', this.onCursorMove)
    this.canvas.off('down', this.onCursorDown)
    this.canvas.off('up', this.onCursorUp)
    this.canvas.off('click', this.onCursorMove)

    this.nodes = []
    this.vertices = []
    this.midpoints = []
  }

  add = (position, node) => {
    this.vertices.push(
      node ||
      new Circle({
        position,
        ctx: this.ctx,
        color: this.color,
        radius: this.radius,
        border: this.border
      })
    )
    this.changed()
  }

  insert = (node) => {
    const i = this.midpoints.findIndex(n => n === node)

    const item = new Circle({
      position: node.position,
      ctx: this.ctx,
      color: this.color,
      radius: this.radius,
      border: this.border
    })

    this.vertices.splice(i + 1, 0, item)

    this.changed()
    this.canvas.gestures.focusing = item
    item.state = 'focus'
  }

  update = () => {
    let midpoints = []
    for (let i = 1; i < this.vertices.length; i++) {
      midpoints.push(
        new Circle({
          position: midpoint(this.vertices[i - 1].position, this.vertices[i].position),
          ctx: this.ctx,
          color: this.color,
          radius: this.radius / 2,
          border: this.border,
          is: 'midpoint'
        })
      )
    }
    this.midpoints = midpoints
    this.nodes = [...this.vertices, ...this.midpoints]
  }

  changed = () => {
    this.update()
    this.emit('change', { target: this })
  }

  render = () => {
    if (this.vertices.length < 1) return

    const { ctx } = this
    ctx.beginPath()
    ctx.strokeStyle = this.color
    ctx.lineWidth = this.lineWidth

    const { x, y } = this.vertices[0].position
    ctx.moveTo(x, y)

    this.vertices.forEach(({ position: { x, y } }) => ctx.lineTo(x, y))
    ctx.stroke()
    ctx.closePath()

    if (typeof this.effect === 'function') this.effect()

    this.nodes.forEach(n => n.render())
  }

  /**
   * draw a white line while drawing state
   */
  whitelineEffect = (position, condition) => {
    if (this.state !== 'drawing') return

    const { ctx, vertices } = this
    const len = vertices.length

    if (len < 1) return

    ctx.beginPath()
    ctx.strokeStyle = '#FFF'
    ctx.lineWidth = this.lineWidth * 0.6
    const last = vertices[len - 1]
    const { x, y } = last.position
    ctx.moveTo(x, y)

    let xp = position.x
    let yp = position.y

    const point = this.find(n => (
      (n.above(position)) &&
      n.is !== 'midpoint' &&
      (!condition || condition(n))
    ))

    if (point) {
      xp = point.position.x
      yp = point.position.y
    }

    ctx.lineTo(xp, yp)
    ctx.stroke()
    ctx.closePath()

    return point
  }
}
