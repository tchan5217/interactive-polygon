### class Polygon
#### events
- change
- close
#### handle events
- polygon.on(evtName, callback)
- polygon.off(evtName, callback)
- polygon.once(evtName, callback)

#### members
vertices - Array of Circle Objects

#### method
#### draw
start drawing
#### pinDraw
return a callback that add node to canvas center everytime it execute.
useful on advanced use or mobile

### class Circle
#### members (get, set)
- position
- color
- radius
- border
- state
- is // if (is === 'midpoint') it is a mid-point of vertice

### class Canvas
#### constructor(domListenTo, domRenderTo)
both args are required, and they must be <canvas>

#### method
##### addNode
canvas.addNode(new Polygon())
#### removeNode
opposite of addNode
#### clear
discards all its child
#### destruct
remove evt listener and discards all its child
